<?php get_header(); ?>
		<!-- CONTENT -->
		<div class="content_page" style="background:#262626;  ">

		<ul class="breadcrumbs">
    		<?php if(function_exists('bcn_display_list')) { bcn_display_list(); }?>
        </ul> 


	<div class="omini_park">

			<div class="title" style='color:white;padding:0px;margin:0px;'><h1 style="padding:0px;margin:0px;color:#6dc5f0;padding-top:10px;"><?php the_title(); ?></h1></div>
			<div class="body">
							<?php 
$posts = get_posts( array(
	'numberposts'     => -1, // тоже самое что posts_per_page
	'offset'          => 0,
	'category_name'        => 'proekty',
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'include'         => '',
	'exclude'         => '',
	'meta_key'        => '',
	'meta_value'      => '',
	'post_type'       => 'post',
	'post_mime_type'  => '', // image, video, video/mp4
	'post_parent'     => '',
	'post_status'     => 'publish'
) );

foreach( $posts as $post ){
	setup_postdata($post);
	if ($post->ID <> $real_id){
	
	?>
				<div class="park">
					
					<div class="img_park"><?php the_post_thumbnail(array(300,225)); ?></div>
					<div class="title_park"><h4><?php the_title(); ?></h4></div>
					<div class="body_park hyphenate text"><span><?php the_excerpt(); ?></span> </div>
					<div class="more_park"><a href="<?php the_permalink(); ?>">Читать далее</a></div>
				</div>
<?php
}
}
wp_reset_postdata();
?>
</div>
</div>
</div>

		
<?php get_footer(); ?>