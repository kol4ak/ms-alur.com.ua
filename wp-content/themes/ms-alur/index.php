	<?php get_header(); ?>

		
		<div class="slider">
			<?php if(sb_slides_display()){sb_slides_display();} ?>

				<!-- <?php //echo do_shortcode("[huge_it_slider id='1']"); ?> 
				
				<!-- <img src=" <?php //bloginfo("template_url"); ?>/i/sl1.png" alt=""> -->
		
		</div> 
		<!-- CONTENT -->
		<div class="content">

		<div class="block block-1">
			<div class="block_margin">
			<div class="title">Выбор<br>Макета</div>
			<div class="step1"></div>
			<div class="images"><img src="<?php bloginfo("template_url"); ?>/i/block1.jpg" alt=""></div>
			<div class="body"> <div class="choose_maket">
				<span>Для выбора макета необходимо определиться со следующими параметрами:</span>
				<?php wp_nav_menu(array('menu_maket' => "Выбор макета")); ?>
				<!-- <a href="">Тип макета </a>
				<a href=""><br>Масштаб</a>
				<a href=""><br>Габариты</a>
				<a href=""><br>Детализация</a>
				<a href=""><br>Подсветка</a> -->
			</div> </div>
			<!-- <div class="more"><a href="">Подробне</a></div> -->
		</div>
		</div>
		<div class="block block-2">
			<div class="block_margin">
			<div class="title">Цена<br>макета</div>
			<div class="step2"></div>
			<div class="images"><img src="<?php bloginfo("template_url"); ?>/i/block2.jpg" alt=""></div>
			<div class="body "><span class="hyphenate text" style="text-align:justify">Для определения предварительной цены макета необходим следующий комплект документации, который зависит от типа макета  </span></div>
			<div class="more"><a href="http://ms-alur.com.ua/price">Подробнее</a></div>
		</div>
		</div>
		<div class="block block-3">
			<div class="block_margin">
			<div class="title">Необходимая документация</div>
			<div class="images"><img src="<?php bloginfo("template_url"); ?>/i/block3.jpg" alt=""></div>
			<div class="body"><span>Для изготовления макета нам потребуется:</span></div>
			<div class="more"><a href="http://ms-alur.com.ua/neobxodimaya-dokumentaciya">Подробнее</a></div>
		</div>
		</div>
		<div class="block block-4">

			<div class="block_margin">
			<div class="title">О мини-парках</div>
			<div class="body">
							<?php 
$posts = get_posts( array(
	'numberposts'     => 2, // тоже самое что posts_per_page
	'offset'          => 0,
	'category_name'        => 'omini',
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'include'         => '',
	'exclude'         => '',
	'meta_key'        => '',
	'meta_value'      => '',
	'post_type'       => 'post',
	'post_mime_type'  => '', // image, video, video/mp4
	'post_parent'     => '',
	'post_status'     => 'publish'
) );

foreach( $posts as $post ){
	setup_postdata($post);
	if ($post->ID <> $real_id){
	
	?>
				<div class="mini_park mini_park_node-1">
					
					<div class="img_park"><?php the_post_thumbnail(array(190,120)); ?></div>
					<div class="title_park"><h3><?php the_title(); ?></h3></div>
					<div class="body_park hyphenate text" style='text-align:justify'><span><?php the_excerpt(); ?></span> </div>
					<div class="more_park"><a href="<?php the_permalink(); ?>">Читать далее</a></div>
				</div>
<?php
}
}
wp_reset_postdata();
?>

				
			</div>
			<div class="more"><a href="http://ms-alur.com.ua/o-mini-parkax">Показать все...</a></div>
		</div>
		</div>

		<div class="block block-5">

			<div class="block_margin">
			<div class="title">Наши проекты</div>
			<div class="body">
				<?php 
$posts = get_posts( array(
	'numberposts'     => 3, // тоже самое что posts_per_page
	'offset'          => 0,
	'category_name'        => 'proekty',
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'include'         => '',
	'exclude'         => '',
	'meta_key'        => '',
	'meta_value'      => '',
	'post_type'       => 'post',
	'post_mime_type'  => '', // image, video, video/mp4
	'post_parent'     => '',
	'post_status'     => 'publish'
) );

foreach( $posts as $post ){
	setup_postdata($post);
	if ($post->ID <> $real_id){
	
	?>

				<div class="our_project">
					<div class="img_project"><?php the_post_thumbnail(array(190,120)); ?></div>
					<div class="title_project">
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					</div>
					<!-- <div class="date_project">10.05.15</div> -->
				</div>
<?php
}
}
wp_reset_postdata();
?>

			</div>
			<div class="more"><a href="http://ms-alur.com.ua/project/">Показать все...</a></div>
		</div>
		</div>

	</div>

	</div>

	<?php get_footer(); ?>