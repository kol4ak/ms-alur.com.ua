<?php get_header(); ?>

	<div id="content">
		<h2 class='page-title'><?php single_cat_title('Рубрика: ' ); ?></h2>
		<?php if (have_posts()): while (have_posts()) :the_post(); ?>
		<div id="record" class="node">
			<div class="feild-title"><h1><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a><span> (<?php the_time('j.m.w' ); ?>)</span></h1></div>
			<div class="feild-image"><?php the_post_thumbnail(array(190,120)); ?></div>
			<div class="feild-body"><span><?php the_excerpt(); ?></div>
			<div class="read_all"><a href="<?php the_permalink('full'); ?>">Читать далее...</a></div>
			<div class="meta"><p><?php the_tags( ); ?></p></div>
		</div>
	<?php endwhile; ?>
	<div class='pagination'>
		<?php

			$big = 999999999; // need an unlikely integer

			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages,
				'prev_text' => '&laquo;',
				'next_text' => '&raquo;'
			) );
		?>
	</div>
<?php endif; ?>

	</div>

<?php get_footer( $name ); ?>