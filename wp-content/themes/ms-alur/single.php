
<?php get_header(); ?>
		<!-- CONTENT -->
		<div class="content_page" style="background:#262626; ">

		<ul class="breadcrumbs">
		     		<?php if(function_exists('bcn_display_list')) { bcn_display_list(); }?>
		 </ul>  -

		<?php if (have_posts()): while (have_posts()) :the_post(); ?>
		<div id="record" class="node" style="margin-left:50px;margin-right:50px;padding-top:0px;">
			<div class="feild-title" style=style='color:white;padding:0px;margin:0px;'><h1 style="padding:0px;margin:0px;color:white;"> <?php the_title(); ?></h1></div>
			<div class="feild-body hyphenate text" style='text-align:justify;color:white;font-size:1.17em; padding-bottom:10px;margin-top:10px;'><span><?php the_content(); ?></div>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
</div>
		</div>

	<?php get_footer(); ?>