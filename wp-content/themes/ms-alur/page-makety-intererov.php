<?php get_header(); ?>
		<!-- CONTENT -->
		<div class="content_page" style="background:#262626;  ">

		<ul class="breadcrumbs">
    		<?php if(function_exists('bcn_display_list')) { bcn_display_list(); }?>
        </ul> 

	<div class="maket_port">

			<div class="title"><?php the_title(); ?></div>
			<div class="body">
				<?php 
$posts = get_posts( array(
	'numberposts'     => -1, // тоже самое что posts_per_page
	'offset'          => 0,
	'category_name'        => 'mi',
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'include'         => '',
	'exclude'         => '',
	'meta_key'        => '',
	'meta_value'      => '',
	'post_type'       => 'post',
	'post_mime_type'  => '', // image, video, video/mp4
	'post_parent'     => '',
	'post_status'     => 'publish'
) );

foreach( $posts as $post ){
	setup_postdata($post);
	if ($post->ID <> $real_id){
	
	?>

				<div class="our_maket">
					<div class="img_maket"><a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail('sidebar-thumb'); ?></a></div>
					<div class="title_maket">
						<h3><?php the_title(); ?></h3>
					</div>
					<div class="body_maket"><span><?php the_excerpt(); ?></span> </div>
					<!-- <div class="date_project">10.05.15</div> -->
				</div>
<?php
}
}
wp_reset_postdata();
?>

			</div>
		</div>
		</div>
	
	<?php get_footer(); ?>