<?php 

/**
* загружаемые стили и скрипты
**/
function load_style_script(){
	wp_enqueue_style('style', get_template_directory_uri().'/style.css');
	wp_enqueue_script('jquery');
}

/**
* загружаемые стили и скрипты
**/

add_action('wp_enqueue_scripts','load_style_script' );
add_theme_support('post-thumbnails');

/*
	Register menu
*/
register_nav_menu('menu','Меню');
register_nav_menu('menu_maket','Выбор макета');
register_nav_menu('menu_mpype','Выбор типа макета');


/*
* сайдбар
*/

register_sidebar(array(
         'name' => 'Виджиты сайдбара',
         'id' => 'sidebar',
         'description' => 'Здесь размещайте виджиты сайдбара',
         'before_widget' => '<div class="rub">',
         'after_widget' => '</div>',
         'before_title' => '<div class="title_kut">',
         'after_title' => '</div>',
	) );

add_image_size( 'sidebar-thumb', 300, 200, true ); // Hard Crop Mode
add_image_size( 'omini-project', 300, 225 ); // Soft Crop Mode
add_image_size( 'singlepost-thumb', 590, 9999 ); // Unlimited Height Mode


 ?>